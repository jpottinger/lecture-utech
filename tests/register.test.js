var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai
var register = require('../src/register');

describe('Register', function () {
    it('Should print out name used to register ', function () {
        expect(register.getName('John')).to.equal('John');
    });
});